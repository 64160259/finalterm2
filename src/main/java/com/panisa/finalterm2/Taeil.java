package com.panisa.finalterm2;

public class Taeil extends Nct implements Singable {
    public Taeil(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }

    @Override
    public String toString() {
        return "Taeil("+this.getUnit()+")" + " national: "+ this.getNational();
    }

}
