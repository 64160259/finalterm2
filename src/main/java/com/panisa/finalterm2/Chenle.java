package com.panisa.finalterm2;

public class Chenle extends Nct implements Singable {
    public Chenle(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }

    @Override
    public String toString() {
        return "Chenle("+this.getUnit()+")" + " national: "+ this.getNational();
    }

}
