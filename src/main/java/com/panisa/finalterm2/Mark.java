package com.panisa.finalterm2;

public class Mark extends Nct implements Rapable {
    public Mark(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public String toString() {
        return "Mark("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
