package com.panisa.finalterm2;

public class Winwin extends Nct implements Danceable {
    public Winwin(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Winwin("+this.getUnit()+")" + " national: " + this.getNational();
    }
}
