package com.panisa.finalterm2;

public class Shotaro extends Nct implements Danceable {
    public Shotaro(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }
    
    @Override
    public String toString() {
        return "Shotaro("+this.getUnit()+")" + " national: " + this.getNational();
    }
}
