package com.panisa.finalterm2;

public class Doyoung extends Nct implements Singable {
    public Doyoung(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocals. ");
    }

    @Override
    public String toString() {
        return "Doyoung("+this.getUnit()+")" + "national: " + this.getNational();
    }
}
