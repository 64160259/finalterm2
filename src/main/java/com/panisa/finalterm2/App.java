package com.panisa.finalterm2;

public class App 
{
    public static void main( String[] args ) {
        System.out.println( "-----Welcome to Nct-----" );

        Taeil tal = new Taeil("nct 127", "korea");
        tal.sing();
        Taeyong yong = new Taeyong("nct 127", "korea");
        yong.rap();
        yong.dance();
        Johnny john = new Johnny("nct 127", "usa");
        john.rap();
        Yuta yu = new Yuta("nct 127", "japan");
        yu.sing();
        yu.dance();
        Kun kun1 = new Kun("wayv", "china");
        kun1.sing();
        Doyoung do1 = new Doyoung("nct 127", "korea");
        do1.sing();
        Ten ten1 = new Ten("wayv", "thai");
        ten1.sing();
        ten1.dance();
        Jaehyun jaeh = new Jaehyun("nct 127", "korea");
        jaeh.sing();
        jaeh.dance();
        Winwin win = new Winwin("nct 127, wayv", "china");
        win.dance();
        Jungwoo joo = new Jungwoo("nct 127", "korea");
        joo.sing();
        joo.dance();
        Lucas luc = new Lucas("wayv", "thai-hongkong");
        luc.rap();
        luc.dance();
        Mark mark1 = new Mark("nct 127, nct dream", "canada");
        mark1.rap();
        Xiaojun jun = new Xiaojun("wayv", "china");
        jun.sing();
        Hendery hen = new Hendery("wayv", "china");
        hen.rap();
        Renjun ren = new Renjun("nct dream", "china");
        ren.sing();
        Jeno jen = new Jeno("nct dream", "korea");
        jen.rap();
        jen.dance();
        Jaemin jaem = new Jaemin("nct dream", "korea");
        jaem.rap();
        jaem.dance();
        Haechan haech = new Haechan("nct 127, nct dream", "korea");
        haech.sing();
        Yangyang yang = new Yangyang("wayv", "taiwan-german");
        yang.rap();
        Shotaro sho = new Shotaro("no fix unit", "japan");
        sho.dance();
        Sungchan sung = new Sungchan("no fix unit", "korea");
        sung.rap();
        Chenle chen = new Chenle("nct dream", "china");
        chen.sing();
        Jisung jis = new Jisung("nct dream", "korea");
        jis.dance();

        
        Singable[] singableObjects = {tal, kun1, yu, do1, ten1, jaeh, joo, jun, ren, haech, chen};
        for(int i=0; i<singableObjects.length;i++) {
            singableObjects[i].sing();
        }

        Rapable[] rapableObjects = {yong, john, luc, mark1, hen, jen, jaem, yang, sung};
        for(int i=0; i<rapableObjects.length;i++) {
            rapableObjects[i].rap();
        }

        Danceable[] danceableObjects = {yong, yu, ten1, jaeh, win, joo, luc, jen, jaem, sho, jis};
        for(int i=0; i<danceableObjects.length;i++) {
            danceableObjects[i].dance();
        }

        System.out.println("-----------");
    }
}
