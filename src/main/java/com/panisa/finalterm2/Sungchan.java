package com.panisa.finalterm2;

public class Sungchan extends Nct implements Rapable {
    public Sungchan(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public String toString() {
        return "Sungchan("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
