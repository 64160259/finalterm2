package com.panisa.finalterm2;

public class Yuta extends Nct implements Singable, Danceable {
    public Yuta(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal. ");
    }
    
    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Yuta("+this.getUnit()+")" + " national: " + this.getNational();
    }
}
