package com.panisa.finalterm2;

public class Jisung extends Nct implements Danceable {
    public Jisung(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Jisung("+this.getUnit()+")" + " national: " + this.getNational();
    }
}
