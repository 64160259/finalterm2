package com.panisa.finalterm2;

public class Jaehyun extends Nct implements Singable, Danceable {
    public Jaehyun(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal. ");
        
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Jaehyun("+this.getUnit()+")" + " national: " + this.getNational();
    }

}
