package com.panisa.finalterm2;

public class Lucas extends Nct implements Rapable, Danceable {
    public Lucas(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Lucas("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
