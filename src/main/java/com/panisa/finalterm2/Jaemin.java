package com.panisa.finalterm2;

public class Jaemin extends Nct implements Rapable, Danceable {
    public Jaemin(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Jaemin("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
