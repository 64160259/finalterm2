package com.panisa.finalterm2;

public class Yangyang extends Nct implements Rapable {
    public Yangyang(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public String toString() {
        return "Yangyang("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
