package com.panisa.finalterm2;

public abstract class Nct {
    private String unit;
    private String national;
    public Nct(String unit, String national) {
        this.unit = unit;
        this.national = national;
    }

    public String getUnit() {
        return unit;
    }

    public String getNational() {
        return national;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setNational(String national) {
        this.national = national;
    }

    @Override
    public String toString() {
        return "Nct unit ("+ unit +") national ("+ national +")";
    }
}
