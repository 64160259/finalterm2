package com.panisa.finalterm2;

public class Jeno extends Nct implements Rapable, Danceable {
    public Jeno(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Jeno("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
