package com.panisa.finalterm2;

public class Jungwoo extends Nct implements Singable, Danceable {
    public Jungwoo(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer. ");
    }

    @Override
    public String toString() {
        return "Jungwoo("+this.getUnit()+")" + " national: " + this.getNational();
    }
}
