package com.panisa.finalterm2;

public class Taeyong extends Nct implements Rapable, Danceable {
    public Taeyong(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void dance() {
        System.out.println(this.toString() + " Dancer.");
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper.");
    }

    @Override
    public String toString() {
        return "Taeyong("+this.getUnit()+")" + " national: "+ this.getNational();
    }
}
