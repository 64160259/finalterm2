package com.panisa.finalterm2;

public class Hendery extends Nct implements Rapable {
    public Hendery(String unit, String national) {
        super(unit, national);
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }

    @Override
    public String toString() {
        return "Hendery("+this.getUnit()+")" + " national: "+ this.getNational();
    }

}
